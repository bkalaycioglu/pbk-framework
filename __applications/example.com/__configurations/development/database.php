<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

$config["database"] = 
	array(
		"teos" => array (
			'type' => 'mysqli',
			'host' => '127.0.0.1',
			'port' => '3306',
			'user' => 'root',
			'pass' => '123456',
			'name' => 'test'
		),
		"teos2" => array (
			'type' => 'postgres',
			'host' => 'medya.ydyazilim.com',
			'port' => '5432',
			'user' => 'lms',
			'pass' => '123456',
			'name' => 'lms'
		)
	);
	