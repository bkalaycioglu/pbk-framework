<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

$config["Routing"] = array(
	"homePage" => array( 
		"method" => "GET", 
		"pattern" => "/",
		"defaultPage" => "Dashboard",
		"defaultController" => "Index",
		"function" => function($args) {
		}
	),
	"pagePattern" => array( 
		"method" => "GET", 
		"pattern" => "/:page/",
		"function" => function($args) {
			
			var_dump($args);
			echo "Hello";
		}
	),
	"pageAndControllerPattern" => array( 
		"method" => "GET", 
		"pattern" => "/:page/:controller/",
		"function" => function($args) {
			
		}
	),
	"exampleController2" => array( 
		"method" => "GET", 
		"pattern" => '/:page/:controller/:action/:id/',
		"function" => function($args) {
		}
	),
	"exampleController" => array( 
		"method" => "GET", 
		"pattern" => '/:page/:controller/:action/:day/:month/:year/',
		"function" => function($args) {
        	var_dump( $args );
        	echo $args["year"];
		}
	),
	"404" => array( 
		"method" => "GET", 
		"pattern" => ":error",
		"function" => function($args) {
			echo "Hello World";
		}
	),
);
