<?php

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

# Timezone is configured
date_default_timezone_set('Europe/Istanbul');

# Define environment type
define("ENVIRONMENT", "development");

# Global function for defining paths
function setSystemPath($name, $path, $definitionName) {
    $path = rtrim(( realpath($path) . "/"), "/") . "/";
    if (!is_dir($path)) {
        die("Your {$name} folder path does not appear to be set correctly.");
    } else {
        define($definitionName, $path);
    }    
}

# Global function for file including if exists.
function ifExistIncludeOnce($folderName, $fileName) {
	if ( ENVIRONMENT != "production" ) {
		$file = "{$folderName}" . ENVIRONMENT . "/{$fileName}";
	    if (!file_exists($file)) {
			$file = "{$folderName}{$fileName}";
		    if (!file_exists($file))
		    	return false;
	    }
	} else {
		$file = "{$folderName}{$fileName}";
	    if (!file_exists($file))
	    	return false;
	}
	return include_once($file);
}

#  Path to the system folder
setSystemPath("Root", "../", "ROOT");

#  Path to the system folder
setSystemPath("pBK", "../__pbk/", "BASEPATH");

#  Path to the system folder
setSystemPath("System", "../__system/", "SYSTEMPATH");

#  Path to the architecture folder
setSystemPath("Architecture", "../__architectures/", "ARCPATH");

#  Path to the public html folder
setSystemPath("Public HTML", __DIR__, "PUBLIC_HTML");

# project-bk is starting.
include_once BASEPATH . "pbk.php";

# Get default application theme name if THEMENAME not setted.
if ( !defined("THEMENAME") ) {
	define("THEMENAME", "/templates/default/");
}

# theme is flush to the screen.
include_once THEMENAME . "template.php";