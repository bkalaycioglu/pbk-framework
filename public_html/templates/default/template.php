<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="shortcut icon" href="<?=THEMENAME;?>favicon.ico">
      <title>Project BK</title>
      <!-- Bootstrap core CSS -->
      <link href="<?=THEMENAME;?>css/bootstrap/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="<?=THEMENAME;?>css/style.css" rel="stylesheet">
   </head>
   <body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">Project BK</a>
            </div>
            <div class="navbar-collapse collapse">
               <ul class="nav navbar-nav">
                  <li class="active"><a href="#">Home</a></li>
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown">About pBK <b class="caret"></b></a>
                     <ul class="dropdown-menu">
                        <li><a href="#">What is pBK</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Using Entity Framework</a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <div class="container-fluid theme-showcase" role="main">
         
      </div>
      <!-- Bootstrap core JavaScript -->
      <!-- ================================================== -->
      <!-- Placed at the end of the document so the pages load faster -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="<?=THEMENAME;?>js/bootstrap/bootstrap.min.js"></script>
   </body>
</html>