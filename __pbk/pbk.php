<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

class pBK {

    # pbk instance is setted as private static
	private static $_instance;
    # magic clone function
    public function __clone() { }
    # magic construct function
    public function __construct() { } 
    # getInstance function
	public static function getInstance(){
        if(null === self::$_instance)
            self::$_instance = new pBK();
        return self::$_instance;
	}

    public static function load($type, $className, $params = null) { 
        $types = explode( "/", $type );

        if ( count($types) !== 2 )
            throw new UnexpectedValueException("{$type} is not valid variable for this load function");

        $folderTypes = array("object", "library", "helper", "database", "core", "configuration");
        $folderNames = array("__objects/", "__libraries/", "__helpers/", "__databases/", "__core/", "__configurations/");

        $basePath = (defined('BASEPATH') ? BASEPATH : "");
        $appPath = (defined('APPPATH') ? APPPATH : "");
        $systemPath = (defined('SYSTEMPATH') ? SYSTEMPATH : "");
        $arcPath = (defined('ARCPATH') ? ARCPATH : "");
        $pagePath = (defined('PAGEPATH') ? PAGEPATH : "");

        $pathTypes = array("pbk", "application", "system", "architecture", "page");
        $pathNames = array($basePath, $appPath, $systemPath, $arcPath, $pagePath);

        $folderName = str_replace($folderTypes, $folderNames, $types[1]);
        $pathName = str_replace($pathTypes, $pathNames, $types[0]);

        $fileName = "{$className}.php";

        if ( $types[1] == "configuration" ) 
            return self::getConfiguration("{$pathName}{$folderName}", $className, $params);

        $isCalled = ifExistIncludeOnce("{$pathName}{$folderName}", $fileName);

        if ( !$isCalled ) 
            return false;
        if ( method_exists ( $className, "getInstance" ) ) 
            return $className::getInstance($params);
        else 
            return true;
    }

    protected static function getConfiguration($path, $name, $params = null) {
        $fileName = "{$path}/{$name}.php";
        if ( file_exists($fileName) ) {
            include($fileName); 
            if ( isset($config) && isset($config[$name]) )
                return $config[$name];
        }

        return false;
    }
}

# Core is loaded.
$pBK = pBK::getInstance();

# benchmark class is loading
$benchmark = pBK::load('pbk/helper', "Benchmark");
$benchmark->start("pbk");

# application routing configurations is loading
$routing = pBK::load('system/configuration', "Routing");

var_dump($routing);die;

# application name is setting 
if ( isset($routing[$_SERVER["SERVER_NAME"]]) )
    $applicationName = $routing[$_SERVER["SERVER_NAME"]];
else if  ( isset($routing["*"]) ) 
    $applicationName = $routing["*"];
else
    throw new UnexpectedValueException("Application routing is not setted in system routing configuration file.");

# application path is defined by the information from routing configuration
$appPath = ROOT . "__applications/{$applicationName}/";
setSystemPath("Application Path", $appPath, "APPPATH");

# page routing configurations is loading
$routingConfiguration = pBK::load('application/configuration', "Routing");

# route classes is loading
pBK::load('pbk/core', "Router");

//Create and the router
$router = new Router();
$router->addRoutes($routingConfiguration);
$router->run();

$routingConfiguration = $router->getMainRouting();

$pageName = (isset($routingConfiguration["page"]) ? $routingConfiguration["page"] : "");
$pagePath = APPPATH . "__pages/{$pageName}/";
setSystemPath("Page Path", $pagePath, "PAGEPATH");

$controllerFileName = (isset($routingConfiguration["controller"]) ? $routingConfiguration["controller"] : "index");
define("PAGE", $controllerFileName);

# page is starting.
$pageFilePath = PAGEPATH . strtolower(PAGE) . ".php";

if ( file_exists($pageFilePath) )
    include_once $pageFilePath;
else 
    throw new UnexpectedValueException("Page file is missing");

$methodName = (isset($routingConfiguration["method"]) ? $routingConfiguration["method"] : "index");
$controllerClassName = ucwords($methodName) . "Controller";
$class = new $controllerClassName();
if ( method_exists ($controllerClassName, $methodName ) ) 
    $class->$methodName();

echo "<br />";

echo $benchmark->report("pbk");
die;