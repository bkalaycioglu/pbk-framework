<?php

if (!defined('BASEPATH')) {
     exit('No direct script access allowed'); 
}

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 *
 * Copyright (C) 2005 Baris Kalaycioglu
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program.
 *
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2005 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

class Route
{
    
    private $_method;               // HTTP Method - POST | PUT | GET | DELETE
    private $_pattern;              // Pattern to match (ie. '/user/profile/[id]')
    private $_defaultPage;          // Default page name
    private $_defaultController;    // Default controller name
    private $_optionalFunctions;    // Optional functions to execute
    private $_function;             // Function to call 
    private $_param;                // Parameter for the function

    // args = method, pettern, optional functions, function
    function __construct($args)
    {
        $this->_method = array_shift($args);
        $this->_pattern = array_shift($args);
        $this->_defaultPage = array_shift($args);
        $this->_defaultController = array_shift($args);
        $this->_function = array_pop($args);
        $this->_optionalFunctions = $args;
        $this->_param = array();    
    }

    public function methodMatches($method) {
        if ( $this->_method == $method )
            return true;
        else
            return false;
    }

    public function getParams() {
        return $this->_param;
    }

    public function patternMatches($URI, $queryString = null) {
        //Extract URL params
        preg_match_all('@:([\w]+)@', $this->_pattern, $paramNames, PREG_PATTERN_ORDER);
        $paramNames = $paramNames[0];
        //Convert URL params into regex patterns, construct a regex for this route
        $patternAsRegex = preg_replace_callback('@:[\w]+@', array($this, '_convertPatternToRegex'), $this->_pattern);
        if ( substr($this->_pattern, -1) === '/' ) {
            $patternAsRegex = $patternAsRegex . '?';
        }
        $patternAsRegex = '@^' . $patternAsRegex . '$@';

        $this->_param = array_merge ( $this->_param, array ( "request" => $queryString ) );
        
        $this->_param["page"] = $this->_defaultPage;
        $this->_param["controller"] = $this->_defaultController;

        if ( preg_match($patternAsRegex, $URI, $paramValues) ) {
            array_shift($paramValues);
            foreach ( $paramNames as $index => $value ) {
                $val = substr($value, 1);
                if ( isset($paramValues[$val]) ) {
                    $this->_param[$val] = urldecode($paramValues[$val]);
                }
            }
            return true;
        }
        return false;
    }

    private function _convertPatternToRegex( $matches ) {
        $key = str_replace(':', '', $matches[0]);
        return '(?P<' . $key . '>[a-zA-Z0-9_\-\.\!\~\*\\\'\(\)\:\@\&\=\$\+,%]+)';
    }

    public function run() {
        //Run the optional functions
        foreach ($this->_optionalFunctions as $function) {
            if (is_callable($function))
                call_user_func($function);
        }

        //Run the main function
        if (is_callable($this->_function)) {
            call_user_func_array($this->_function, array( 'params' => $this->_param ) ) ;
            return true;
        }
        return false;
    }

}