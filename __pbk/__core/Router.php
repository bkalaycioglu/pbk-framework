<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

# route classes is loading
pBK::load('pbk/core', "Route");

class Router
{
	private $_routes;
	private $_matchingRoutes;
	private $_defaultRoute;
	
	function __construct()
	{
		// Contains all the routes
		$this->_routes = array();

		// Contains the matching routes
		$this->_matchingRoutes = array();

		// Init default route
		$this->_defaultRoute = NULL;
	}

	public function addRoutes() {
		$args = func_get_args();

		foreach ($args[0] as $name => $configuration) {

			$method = "GET";

			if ( isset($configuration["method"] ) )
				$method = $configuration["method"];

			$pattern = "/";
			if ( isset($configuration["pattern"] ) )
				$pattern = $configuration["pattern"];

			$defaultPage = null;
			if ( isset($configuration["defaultPage"] ) )
				$defaultPage = $configuration["defaultPage"];

			$defaultController = null;
			if ( isset($configuration["defaultController"] ) )
				$defaultController = $configuration["defaultController"];

			$function = null;
			if ( isset($configuration["function"] ) )
				$function = $configuration["function"];	

			$this->addRoute($method, $pattern, $defaultPage, $defaultController, $function);
		}
	}

	public function addRoute() {
		$args = func_get_args();
		array_push($this->_routes, new Route($args));
	}

	// If nothing match the default route apply
	public function setDefaultRoute($route) {
		$this->_defaultRoute = $route;
	}

	//Starting the router with the HTTP infos
	private function getDefaultParamaters() {
		//Empty arrays
	    $inputs = array();

	    //URI Data
	    $scriptName = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME']) ); 
	    $URI = '/'.substr_replace($_SERVER['REQUEST_URI'], '', 0, strlen($scriptName));
	    $URI = str_replace('//', '/', str_replace("?{$_SERVER['QUERY_STRING']}", '', $URI));

	    //Query strings
	    parse_str($_SERVER['QUERY_STRING'], $queryString);

	    // POST - PUT - GET - DELETE
	    $method = @$_SERVER['REQUEST_METHOD'];

	    //Raw input for requests
	    $raw = @file_get_contents('php://input');

		//Find the matching methods
		$this->_findMatchingMethod($this->_routes, $method);

		//In previous matching routes find the ones matching the pattern
		$this->_findMatchingPattern($this->_matchingRoutes, $URI, $queryString);
	}

	//Starting the router with the HTTP infos
	public function run() {
		# load default parameters
		$this->getDefaultParamaters();

		if (count($this->_matchingRoutes) == 0) {
			//If no route match
			if ( !is_null($this->_matchingRoutes) )
				header('Location: '.$this->_defaultRoute);

		} else {
			//Run the matching routes
			foreach ($this->_matchingRoutes as $route) {
				$route->run();
			}
		}
	}

	// Get default page, controller and method name
	public function getMainRouting() {
		if (count($this->_matchingRoutes) == 0) {
			return null;
		} else {
			return $this->_matchingRoutes[0]->getParams();
		}
	}
	private function _findMatchingMethod($routes, $method) {
		foreach ($routes as $route) {
			if ( $route->methodMatches($method) )
				array_push($this->_matchingRoutes, $route);
		}
	}

	private function _findMatchingPattern($routes, $URI, $queryString) {
		//Reset the matching pattern array
		$this->_matchingRoutes = array();
		foreach ($routes as $route) {
			if ($route->patternMatches($URI, $queryString) )
				array_push($this->_matchingRoutes, $route );
		}
	}
}