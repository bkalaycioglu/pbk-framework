<?php

if (!defined('BASEPATH')) {
     exit('No direct script access allowed'); 
}

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

class Query {

    private $query = null; 
    private static $_databaseType = null;
    private static $_queryType = null;
    private static $_tableName = null;
    private static $_fields = array();
    private static $_where = array();
    private static $_having = array();
    private static $_join = array();
    private static $_groupBy = array();
    private static $_orderBy = array();
    private static $_limit = null;
    private static $_offset = null;

    private static $_instance;
    public function __clone() { }
    public function __construct() { } 
    public static function getInstance() {
        self::$_instance = new query();
        return self::$_instance;
    }

    public function newQuery() {
        $this->query = null;
        self::$_databaseType = null;
        self::$_queryType = null;
        self::$_tableName = null;
        self::$_fields = array();
        self::$_where = array();
        self::$_having = array();
        self::$_join = array();
        self::$_groupBy = array();
        self::$_orderBy = array();
        self::$_limit = null;
        self::$_offset = null;
        return self::$_instance;
    }
    public function __invoke() 
    {
        if ( $this->query == null )  {
            $this->getMySQLQuery();
        }
        return (string)$this->query;
    }

    public function from($tableName)
    {
        self::$_tableName = $tableName;
        return $this;
    }

    public function select($fields = null)
    {
        $fields = func_get_args();

        self::$_queryType = "select";

        if ( isset($fields[1]) ) {
            self::$_fields = $fields;
        } else {
            self::$_fields = null;
        }
        return $this;
    }

    public function insert($tableName)
    {
        return $this; 
    }

    public function delete($tableName)
    {

        return $this; 
    }

    public function update($tableName)
    {
        return $this; 
    }

    private function getQueryFilterData($fieldName, $value, $operation, $tableName)
    {
        $query = array();
        $query["type"] = "field";
        $query["fieldName"] = $fieldName;
        $query["value"] = $value;
        $query["operation"] = $operation;
        return $query;
    }

    public function having($fieldName, $value, $operation = "equals", $tableName = null)
    {
        $havingQuery = $this->getQueryFilterData($fieldName, $value, $operation, $tableName);
        self::$_having[] = $havingQuery;
        return $this;
    }

    public function where($fieldName, $value, $operation = "equals", $tableName = null)
    {
        $whereQuery = $this->getQueryFilterData($fieldName, $value, $operation, $tableName);
        self::$_where[] = $whereQuery;
        return $this;
    }

    public function join($tableName, $fieldNameA, $fieldNameB, $joinType = "inner", $operation = "equals")
    {
        $joinQuery = array();
        $joinQuery["type"] = strtoupper($joinType);
        $joinQuery["fieldNameA"] = $fieldNameA;
        $joinQuery["fieldNameB"] = $fieldNameB;
        $joinQuery["operation"] = $operation;
        $joinQuery["tableName"] = $tableName;

        self::$_join[] = $joinQuery;
        return $this;
    }

    private function getQueryFilterOperationData($type)
    {
        $query = array();
        $query["type"] = $type;

        return $query;
    }

    public function groupBy()
    {
        return $this;
    }

    public function limit($limit)
    {
        if ( $limit < 0 )
            $limit = null;

        self::$_limit = $limit;
        return $this;
    }

    public function offset($offset)
    {
        if ( $offset < 0 )
            $offset = null;

        self::$_offset = $offset;
        return $this;
    }


    public function orderBy()
    {
        return $this;
    }

    public function getMySQLQuery()
    {
        $query = "";
        if ( self::$_queryType == "select" ) {
            $query .= "SELECT ";
            if (self::$_fields == null) {
                $query .= "*";
            } else {
                $sayac = 0;
                foreach ( self::$_fields as $key => $fieldName) { 
                    if ( $sayac > 0 ) {
                        $query .= ", ";  
                    }
                    $query .= $fieldName; 
                    $sayac = 1; 
                }
            }

            $query .= " FROM " . self::$_tableName;

            if ( count( self::$_join ) > 0 )
            {
                foreach(self::$_join as $key => $value) {

                    if ( $value["type"]  == "INNER") {
                        $query .= " INNER JOIN ";
                    } elseif ( $value["type"]  == "LEFT") {
                        $query .= " RIGHT JOIN ";
                    } elseif ( $value["type"]  == "RIGHT") {
                        $query .= " LEFT JOIN ";
                    } else {
                        $query .= " INNER JOIN ";
                    }

                    $query .= "{$value["tableName"]} ON ";
                    
                    if ( $value["operation"] == "equals" ) {
                        $query .= $value["fieldNameA"] . " = " . $value["fieldNameB"] . " ";
                    }
                }
            }

            if ( count( self::$_where ) > 0 )
            {
                $query .= " WHERE ";
            }

            $sayac = 0;
            $fieldGorunduMu = false;

            foreach(self::$_where as $key => $value) {
                if ( $sayac > 0 ) {
                    if ( $fieldGorunduMu && $value["type"]  == "field") {
                         $query .= " AND ";
                    }
                }

                if ( $value["type"]  == "field" ) {
                    $fieldGorunduMu = true;
                    if ( $value["operation"] == "equals" ) {
                        $query .= $value["fieldName"] . " = '" . $value["value"] . "' ";
                    }
                    if ( $value["operation"] == "like" ) {
                        $query .= $value["fieldName"] . " LIKE ('" . $value["value"] . "') ";
                    }

                    if ( isset($value["tableName"]) AND !is_null($value["tableName"]) ) 
                        $query = $value["tableName"] . $query;
                }

                $sayac += 1;
            }

            if ( self::$_limit != null )
            {
                $query .= " LIMIT " . self::$_limit;
            }

            if ( self::$_offset != null )
            {
                $query .= " OFFSET " . self::$_offset;
            }
        }
        $this->query = $query;
        return $this;
    }
    
    public function toString() 
    {
        return new String($this->query);
    }

}



