<?php

if (!defined('BASEPATH')) {
     exit('No direct script access allowed'); 
}

/**
 * pBK FrameWork
 *
 * Project BK design pattern can speed up the development process by providing tested, proven development paradigms.
 * 
 * Copyright (C) 2006 Baris Kalaycioglu
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public 
 * License as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied 
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. 
 * 
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     pBK
 * @author      Baris Kalaycioglu
 * @copyright   Copyright (c) 2006 - 2013, Baris Kalaycioglu
 * @license     http://www.bariskalaycioglu.com/pbk/user_guide/license.html
 * @link        http://www.bariskalaycioglu.com/pbk/
 *
 */

class Benchmark {
    private static $_timer;
    # pbk instance is setted as private static
    private static $_instance;
    # magic clone function
    public function __clone() { }
    # magic construct function
    public function __construct() { } 
    # getInstance function
    public static function getInstance() {
        if(null !== self::$_instance)
            return self::$_instance;
        self::$_instance = new Benchmark();
        return self::$_instance;
    }

    /*  start the timer  */
    public function start($whichOne) {
        self::$_timer[$whichOne] = new stdClass;
        self::$_timer[$whichOne]->start = $this->get_time();
        self::$_timer[$whichOne]->isEnd = FALSE;
    }

    /*  pause the timer  */
    public function stop($whichOne) {
        if ( !isset(self::$_timer[$whichOne]) )
            return false;
        self::$_timer[$whichOne]->isEnd = TRUE;
        self::$_timer[$whichOne]->end = $this->get_time();
    }

    /*  get the current timer value  */
    public function report($whichOne, $decimals = 8) {
        if ( !isset(self::$_timer[$whichOne]) )
            return false;
        if ( self::$_timer[$whichOne]->isEnd ) 
            return round((self::$_timer[$whichOne]->end - self::$_timer[$whichOne]->start),$decimals);
        else 
            return round(($this->get_time() - self::$_timer[$whichOne]->start),$decimals);
    }

    /*  format the time in seconds  */
    private function get_time() {
        list($usec,$sec) = explode(' ', microtime());
        return ((float)$usec + (float)$sec);
    }
}